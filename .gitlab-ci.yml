---
image:
  name: hashicorp/terraform:0.15.1
  entrypoint:
    - /usr/bin/env

variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: deploy/code

cache:
  paths:
    - .terraform
    - ${TF_ROOT}/.terraform

stages:
  - lint
  - test
  - plan
  - apply
  - destroy
  - infracost

yamllint:
  image: sdesbure/yamllint
  stage: lint
  before_script:
    - cd ${TF_ROOT}
  script:
    - yamllint .

fmt-check:
  stage: lint
  before_script:
    - cd ${TF_ROOT}
  script:
    - terraform fmt -recursive

validate:
  stage: test
  before_script:
    - cd ${TF_ROOT}
    #- echo $tfc_credentials > /root/.terraformrc
  script:
    - terraform init
    - terraform version
    - terraform validate

plan:
  stage: plan
  before_script:
    - mkdir ${CI_PROJECT_DIR}/tfplan
    - cd ${TF_ROOT}
    #- echo $tfc_credentials > /root/.terraformrc
  script:
    - terraform init
    - terraform plan -out ${CI_PROJECT_DIR}/tfplan/out.tfplan
    - terraform show -json ${CI_PROJECT_DIR}/tfplan/out.tfplan > ${CI_PROJECT_DIR}/tfplan/tfplan.json
  artifacts:
    expire_in: 8 weeks
    paths:
      - ${CI_PROJECT_DIR}/tfplan/out.tfplan
      - ${CI_PROJECT_DIR}/tfplan/tfplan.json

apply:
  stage: apply
  before_script:
    - cd ${TF_ROOT}
    #- echo $tfc_credentials > /root/.terraformrc
  script:
    - terraform apply -input=false ${CI_PROJECT_DIR}/tfplan/out.tfplan
  when: manual
  only:
    - main

destroy:
  stage: destroy
  before_script:
    - cd ${TF_ROOT}
    #- echo $tfc_credentials > /root/.terraformrc
  script:
    - terraform init
    - terraform destroy -auto-approve
  when: manual
  only:
    - main

include:
  # Use a specific version of the template instead of master if locking the template is preferred
  remote: 'https://gitlab.com/infracost/infracost-gitlab-ci/-/raw/master/infracost.yml'

infracost-plan:
  stage: infracost
  extends: .infracost
  only:
    - merge_requests

infracost-report:
  stage: infracost
  before_script:
    - mkdir ${CI_PROJECT_DIR}/report
  image:
    # Always use the latest 0.9.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.9
    entrypoint: [""] # Override since we're running commands below
  dependencies:
    - plan
  script:
    - infracost breakdown --path ${CI_PROJECT_DIR}/tfplan/tfplan.json --format json --out-file ${CI_PROJECT_DIR}/report/infracost.json
    - infracost output --path ${CI_PROJECT_DIR}/report/infracost.json --format html > ${CI_PROJECT_DIR}/report/infracost_report.html
  artifacts:
    expire_in: never
    paths:
      - ${CI_PROJECT_DIR}/report/infracost.json
      - ${CI_PROJECT_DIR}/report/infracost_report.html  
  variables:
      INFRACOST_API_KEY: $INFRACOST_API_KEY
      GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
      #COMMENT_FORMAT: gitlab-comment
      # Choose the commenting behavior, 'update' is a good default:
      # update: Create a single comment and update it. The "quietest" option.
      # delete-and-new: Delete previous comments and create a new one.
      #  new: Create a new cost estimate comment on every push.
      # COMMENT_BEHAVIOR: update
      # Limit the object that should be commented on, either merge-request or commit
      # COMMENT_TARGET_TYPE: merge-request

      # A JSON string describing the condition that triggers merge request comments, can be one of these:
      #  - `'{"has_diff": true}'`: only post a comment if there is a diff. This is the default behavior.
      #  - `'{"always": true}'`: always post a comment.
      #  - `'{"percentage_threshold": 0}'`: absolute percentage threshold that triggers a comment. 
      # For example, set to 1 to post a comment if the cost estimate changes by more than plus or minus 1%.
      # post_condition: '{"has_diff": true}'
  only:
    - merge_requests

  #when: manual
  #only:
  #  - main
  
