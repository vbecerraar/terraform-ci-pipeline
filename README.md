## Reference pipeline to deploy terraform on AWS
 - Using gitlab pipeline (this project)
 - Using aws codepipiline : folder ./terraform-workflow-codepipeline

# Terraform sample code ./deploy/code

## Setup AWS variables in your gitlab project

Setting the "AWS_PROFILE", "AWS_ACCESS_KEY_ID" and "AWS_SECRET_ACCESS_KEY" environment variables on Setting > CI/CD

## .pre-commit-config.yaml
# Hooks
- Using pre-commit-terraform hooks from gruntwork-io and from antonbabenko to test,validate and scan terraform code
- Also could use infracost on pre-commit-config

## Gitlab pipeline stages and hooks description

- Using default hashicorp/terraform image set to 15.1
(non intended for production, you need to deploy a custom golden docker image with the versions tools that your environment need)
# Sample : 
  terraform, pre-commit, git, yamlint, etc

- Using gitlab cache to improve terraform module/pluging load

# Stages
- lint (yaml, terraform)
- test (init included in test stage)
- plan
- apply
- destroy
- infracost


## - Introduce Infracost hook 

https://gitlab.com/infracost/infracost-gitlab-ci/

https://gitlab.com/guided-explorations/iac/infracost-for-terraform

https://www.infracost.io/docs/integrations/cicd/#gitlab-ci

# Install locally infracost in your laptop and generare one time API KEY.

 $brew install infracost
 
 $infracost register

 The key is saved on ~/.config/infracost/credentials.yml

 Setup an environment variable "INFRACOST_API_KEY" with your API key in your gitlab project.

[Test Infracost report generated on html]:

https://vbecerraar.gitlab.io/-/terraform-ci-pipeline/-/jobs/2018612217/artifacts/report/infracost_report.html

https://vbecerraar.gitlab.io/-/terraform-ci-pipeline/-/jobs/2019000882/artifacts/report/infracost_report.html

# Working with multiple branches
- By default "Protect variable" checkbox will export the variable to only pipelines running on protected branches and tags.
If you have more branches like "develop" apart of the "main" one, be sure to setup that branch as protected , otherwise the AWS credentials will not be located.

https://about.gitlab.com/blog/2021/02/05/ci-deployment-and-environments/#keeping-secret-things-secret

There are more options to setup AWS ENV variables on gitlab, bellow is a great one.

GitLab CI: How to Manage AWS CLI Credentials for multiple AWS Accounts in a Single Pipeline :

 https://medium.com/devops-with-valentine/gitlab-ci-how-to-manage-aws-cli-credentials-for-multiple-aws-accounts-in-a-single-pipeline-bb2b37972999





 
